import { BaseEntity, Column, ManyToOne } from "typeorm";
import { User } from "./user.entity";

export class IOperation extends BaseEntity {

    static OutOfDate = 1;
    static Unactive = 2;
    static Active = 3;
    
    @Column()
    expiry_date:Date;

    @Column()
    creation_date:Date;

    @Column()
    is_active:boolean;

    @Column({ nullable: false })
    readonly userId:number;

    @ManyToOne(
        type => User,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    user:User;

    test ():number {
        
        let now = new Date();
        if (this.creation_date > now || this.expiry_date < now) {
            console.log('outOfTime', this.creation_date, now, this.expiry_date)
            return IOperation.OutOfDate;
        }
        if (!this.is_active || !this.user.is_active) {
            return IOperation.Unactive;
        }
        
        return IOperation.Active;
    }
}