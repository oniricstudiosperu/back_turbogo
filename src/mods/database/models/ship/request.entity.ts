import { Entity, Column, PrimaryGeneratedColumn, Index, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { IActivable } from '../iactivable';
import { User } from '../user.entity';

export enum ShipRequestType {
    Requester = 'requester',
    Requesting = 'requesting'
}

@Entity('ship_request')
export class Request extends IActivable {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    creation_date:Date;

    @Column({ nullable: false })
    creationUserId:number;

    @Column({ nullable: false })
    targetId:number;

    @ManyToOne(
        type => User,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    @JoinColumn ({ name: 'creationUserId' })
    creation_user:User;

    @Column ({
        type: 'enum',
        enum: ShipRequestType
    })
    type:ShipRequestType;

    @ManyToOne(
        type => User,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    @JoinColumn ({ name: 'targetId' })
    target:User;

    static async formatFind (
        id:number,
        type: 'target' | 'creator' | 'both',
        shipId?:number
    ) {

        let $or = [];
        
        if (type == 'target' || 'both') {
            let aux:any = { target: id, active: true };
            if (shipId) aux.creation_user = shipId;
            $or.push (aux)
        }
        if (type == 'creator' || 'both') {
            let aux:any = { creation_user: id, active: true };
            if (shipId) aux.target = shipId;
            $or.push (aux)
        }
        return await Request.find ({
            where: { $or }
        })
    }

    async generateShip () {

        await User.createQueryBuilder()
        .relation (User, 'ships')
        .of (this.creation_user)
        .add (this.target);

        await User.createQueryBuilder()
        .relation (User, 'ships')
        .of (this.target)
        .add (this.creation_user);

    }
    
}