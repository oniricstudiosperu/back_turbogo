import { Entity, Column, PrimaryGeneratedColumn, Index, BaseEntity } from 'typeorm';

@Entity('mail')
export class Mail extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    from:string;

    @Column({ length: 255 })
    to:string;

    @Column({ length: 500 })
    subject:string;

    @Column({ type: 'text' })
    body:string;

    @Index()
    @Column ()
    is_sent:boolean;
    
}