import { Entity, Column, PrimaryGeneratedColumn, Index, BaseEntity } from 'typeorm';

@Entity('sms')
export class Sms extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    from:string;

    @Column({ length: 255 })
    to:string;

    @Column({ type: 'text' })
    body:string;

    @Index()
    @Column ()
    is_sent:boolean;
    
    @Column({ length: 255 })
    error:string;
}