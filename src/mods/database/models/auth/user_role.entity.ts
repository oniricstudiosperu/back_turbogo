import { Entity, Column, PrimaryGeneratedColumn, Index, ManyToMany, JoinTable, BaseEntity, ManyToOne } from 'typeorm';
import { User } from '../user.entity';
import { Role } from './role.entity';

@Entity('auth_user_role')
export class UserRole extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    creation_date:Date;

    @Column()
    start_date:Date;

    @Column()
    end_date:Date;

    @Column()
    is_active:boolean;

    @Column({ nullable: false })
    readonly userId:number;

    @Column({ nullable: false })
    readonly roleId:number;

    @ManyToOne(
        type => User,
        {
            nullable: false,
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        }
    )
    user:User;

    @ManyToOne(
        type => Role,
        {
            nullable: false,
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
        }
    )
    role:Role;
    
}