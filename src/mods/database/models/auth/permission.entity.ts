import { Entity, Column, PrimaryGeneratedColumn, Index, ManyToMany, BaseEntity, OneToMany } from 'typeorm';
import { Role } from './role.entity';
import { Menu } from './menu.entity';

@Entity('auth_permission')
export class Permission extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 250 })
    name: string;

    @Column({ length: 500 })
    description: string;

    @Column()
    creation_date: Date;

    @Column()
    is_active: boolean;

    @ManyToMany(
        type => Role,
        role => role.permissions
    )
    roles:Role[];

    @OneToMany (
        type => Menu,
        menu => menu.permission
    )
    menus:Permission[];

}