import { Entity, Column, PrimaryGeneratedColumn, Index, ManyToMany, JoinTable, BaseEntity } from 'typeorm';
import { Permission } from './permission.entity';

@Entity('auth_role')
export class Role extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 250 })
    name: string;

    @Column({ length: 500 })
    description: string;

    @Column()
    creation_date: Date;

    @Column()
    is_active: boolean;

    @ManyToMany(
        type => Permission,
        permission => permission.roles
    )
    @JoinTable()
    permissions:Permission[];

}