import { Entity, Column, PrimaryGeneratedColumn, Index, ManyToMany, JoinTable, BaseEntity, ManyToOne } from 'typeorm';
import { Permission } from './permission.entity';

@Entity('auth_menu')
export class Menu extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 250 })
    name: string;

    @Column({ length: 500 })
    description: string;

    @Column()
    creation_date: Date;

    @Column()
    is_active: boolean;

    @Column({ nullable: false })
    readonly permissionId:number;

    @ManyToOne(
        type => Permission,
        permission => permission.menus,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    permission:Permission[];

}