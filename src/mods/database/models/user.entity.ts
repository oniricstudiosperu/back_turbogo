import { Entity, Column, PrimaryGeneratedColumn, Index, OneToMany, ManyToOne, BaseEntity, QueryBuilder, MoreThan, ManyToMany, JoinTable } from 'typeorm';
import { Role } from './auth/role.entity';
import { Menu } from './auth/menu.entity';
import { Permission } from './auth/permission.entity';
import { Validate, ValidateTarget } from './op/validate.entity';
import { SettingsService } from '../prov/settings/settings.service';
import { EnterpriseRoleUser } from './bus/enterprise_role_user.entity';
import { IActivable } from './iactivable';
import { Enterprise } from './bus/enterprise.entity';
import { Delimit } from 'src/classes/delimit.class';
import { Owner } from './tide/owner.entity';
import { Request } from './ship/request.entity';
const bcrypt = require ('bcrypt');

@Entity('user')
export class User extends IActivable {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 250 })
    firstname: string;

    @Column({ length: 250 })
    lastname: string;

    @Index()
    @Column ({ length: 255, nullable: true })
    account:string;

    @Index()
    @Column ({ length: 255, nullable: true })
    email:string;

    @Index()
    @Column ({ length: 20, nullable: true })
    phone:string;

    @Column ()
    is_email_valid:boolean;

    @Column ()
    is_phone_valid:boolean;

    @Column ({ type: 'text' })
    password:string;

    @Column()
    creation_date:Date;

    @Column({ nullable: false })
    readonly tideOwnerId:number;

    @Column({ nullable: false })
    readonly roleId:number;
    
    @ManyToOne(
        type => Owner,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    tide_owner:Owner;

    @ManyToOne(
        type => Role,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    role:Role;

    @ManyToMany (
        type => User,
        {
            nullable: false
        }
    )
    @JoinTable ()
    ships:User[];

    @OneToMany (
        type => Request,
        request => request.creation_user
    )
    createdRequest:Request[];

    @OneToMany (
        type => Request,
        request => request.creation_user
    )
    targettedRequest:Request[];

    @OneToMany (
        type => EnterpriseRoleUser,
        enterpriseRoleUser => enterpriseRoleUser.user
    )
    enterpriseRoleUsers?:EnterpriseRoleUser[];

    isActive ():boolean {
        return super.isActive() && (this.is_email_valid || this.is_phone_valid)
    }

    static async findOneBasic (id:string) {
        let user = await User.findOne ({
            where: { id: id },
            relations: [ 'tide_owner' ]
        });
        if (!user) {
            return user;
        }
        if (user.isActive()) {
            delete user.password;
        }
        return user;
    }

    static findByQuery (where?:string, whereData?: { [key:string]:any }) {
        let query = User.getRepository().createQueryBuilder('user');
        if (where) {
            query.where (where, whereData)
        }
        query.leftJoinAndSelect ('user.tide_owner', 'owner')
        return query.andWhere ('user.is_active=:active', { active: true })
        .andWhere ('user.is_email_valid=true or user.is_phone_valid=true');
    }

    static async findBy (where:string, whereData:{ [key:string]:any }) {
        return await User.findByQuery(where, whereData).getMany();
    }

    async setPassword (password) {
        let hash = await bcrypt.hash (password, 10);
        this.password = hash;
    }

    async comparePassword (password) {
        return await bcrypt.compare (password, this.password);
    }

    async getMenu () {
        return await Menu.getRepository ()
        .createQueryBuilder('menu')
        .leftJoin (Permission, 'permission', 'menu.permissionId = permission.id')
        .leftJoin ('permission.roles', 'role', 'role.id=:roleId', { roleId: this.role.id })
        .getMany();
        //.leftJoin ('permission.roles', 'role', 'role.id=:roleId', { roleId: this.role.id });
    }

    private enterpriseQuery(user:User) {
        return Enterprise.getRepository()
        .createQueryBuilder('ent')
        .leftJoinAndSelect (
            'ent.enterpriseRoleUsers',
            'rels')
        .where ('rels.userId=:uIdOne or rels.userId=:uIdOne',
            { uIdOne: user.id, uIdTwo: this.id });
        ;
    }

    async getEnterpriseList (user:User, offset?:number, limit?:number):Promise<Enterprise[]> {
        let query = this.enterpriseQuery(user);

        Delimit.is(offset, () => query.offset (offset));
        Delimit.is(limit, () => query.offset (limit));

        return await query.getMany();
    }

    async getEnterprise (id) {
        let query = this.enterpriseQuery(this);
        query.andWhere ('ent.id=:entId', { entId: id });
        query.leftJoinAndSelect ('ent.tide_owner', 'tide_owner');
        query.leftJoinAndSelect ('tide_owner.profile', 'profile');
        return await query.getOne();
    }

    hideEnterprises(...list:Enterprise[]) {
        let turn = {
            data:[],
            hidden:0,
            length: list.length
        }
        for (let i in list) {
            let flag = false;
            for (let j in list[i].enterpriseRoleUsers) {
                if (list[i].enterpriseRoleUsers[j].userId == this.id) {
                    flag = true;
                }
            }
            if (flag) {
                turn.data.push (list[i]);
            } else {
                turn.hidden++;
                turn.length--;
            }
        }
        return turn;
    }

    async createValidate (settings:SettingsService, target:ValidateTarget) {
        let operation = new Validate();
        operation.generate(settings.sessionExpireTime * 1000);
        operation.target = target;
        operation.user = this;
        return await operation.save();
    }

    async getValidation(settings:SettingsService) {
        let validation = await Validate.findOne ({
            where: {
                user: this,
                is_active: true
            },
            order: {
                creation_date: 'DESC'
            }
        });
        
        if (!validation) {
            throw { error: 'The user was created without a validation' };
        }
        validation.user = this;
        if (validation.test() == Validate.Active) {
            return validation;
        } else {
            return this.createValidate (settings, validation.target);
        }
    }
    
    async findShip (user:User) {
        return await User.getRepository()
        .createQueryBuilder('user')
        .leftJoinAndSelect ('user.ships', 'ship', 'ship.id=:userId', { userId: user.id })
        .where ('user.id=:id', { id: this.id })
        .getOne()
    }
}