import { Entity, Column, PrimaryGeneratedColumn, JoinTable, BaseEntity, ManyToOne } from 'typeorm';
import { EnterprisePermission } from './enterprise_permission.entity';
import { Enterprise } from './enterprise.entity';
import { EnterpriseRole } from './enterprise_role.entity';
import { User } from '../user.entity';

@Entity('bus_enterprise_role_user')
export class EnterpriseRoleUser extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    is_active: boolean;

    @Column({ nullable: true })
    readonly enterpriseId:number;

    @Column({ nullable: true })
    readonly roleId:number;
    
    @Column({ nullable: true })
    readonly userId:number;

    @ManyToOne (
        type => Enterprise,
        enterprise => enterprise.enterpriseRoleUsers,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    enterprise:Enterprise;

    @ManyToOne (
        type => EnterpriseRole,
        role => role.enterpriseRoleUsers,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    role:EnterpriseRole;

    @ManyToOne (
        type => User,
        user => user.enterpriseRoleUsers,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    user:User;

}