import { Entity, Column, PrimaryGeneratedColumn, Index, ManyToMany, BaseEntity, OneToMany } from 'typeorm';
import { EnterpriseRole } from './enterprise_role.entity';

@Entity('bus_enterprise_permission')
export class EnterprisePermission extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 250 })
    name: string;

    @Column({ length: 500 })
    description: string;

    @Column()
    creation_date: Date;

    @Column()
    is_active: boolean;

    @ManyToMany(
        type => EnterpriseRole,
        role => role.permissions
    )
    roles:EnterpriseRole[];

    static async selectByRole (roleId) {
        return await EnterprisePermission.getRepository()
            .createQueryBuilder('permission')
            .leftJoinAndSelect("permission.roles", "role")
            .where ('role.id=:idRole', { idRole: roleId })
            .getMany()
    }

    static async isIn (value:string, permissions:EnterprisePermission[]) {
        for (let i in permissions) {
            if (permissions[i].is_active && permissions[i].name == value) {
                return true;
            }
        }
        return false;
    }

}