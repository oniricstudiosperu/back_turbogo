import { Entity, Column, PrimaryGeneratedColumn, Index, ManyToMany, JoinTable, BaseEntity, OneToMany } from 'typeorm';
import { EnterprisePermission } from './enterprise_permission.entity';
import { EnterpriseRoleUser } from './enterprise_role_user.entity';

@Entity('bus_enterprise_role')
export class EnterpriseRole extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 250 })
    name: string;

    @Column({ length: 500 })
    description: string;

    @Column()
    creation_date: Date;

    @Column()
    is_active: boolean;

    @ManyToMany(
        type => EnterprisePermission,
        permission => permission.roles
    )
    @JoinTable()
    permissions:EnterprisePermission[];

    @OneToMany (
        type => EnterpriseRoleUser,
        enterpriseRoleUser => enterpriseRoleUser.role
    )
    enterpriseRoleUsers:EnterpriseRoleUser[];

    
    static async findSuper() {
        return await this.findOne(1);
    }

}