import { Entity, Column, PrimaryGeneratedColumn, Index, BaseEntity, OneToMany, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { EnterpriseRoleUser } from './enterprise_role_user.entity';
import { EnterpriseRole } from './enterprise_role.entity';
import { Owner } from '../tide/owner.entity';

@Entity('bus_enterprise')
export class Enterprise extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 250 })
    name: string;

    @Column({ length: 500 })
    description: string;

    @Column ()
    creation_date:Date;

    @Column({ nullable: false })
    readonly tideOwnerId:number;
    
    @ManyToOne(
        type => Owner,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    tide_owner:Owner;

    @Column ()
    is_visible:boolean;

    @OneToMany (
        type => EnterpriseRoleUser,
        enterpriseRoleUser => enterpriseRoleUser.enterprise
    )
    enterpriseRoleUsers:EnterpriseRoleUser[];
    
}