import { Entity, Column, PrimaryGeneratedColumn, Index, ManyToOne, BaseEntity } from 'typeorm';
import { IOperation } from '../ioperation';

export enum ValidateTarget {
    Phone = 'phone',
    Email = 'email'
};

@Entity('op_validate')
export class Validate extends IOperation {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'enum',
        enum: ValidateTarget
    })
    target: ValidateTarget;

    @Column ({ length: 250 })
    code:string

    generate (milliseconds:number) {
        let codeLength = 6;
        let codeString = '1234567890';
        this.creation_date = new Date(Date.now() - 1000);
        this.expiry_date = new Date(Date.now() + milliseconds);
        this.is_active = true;

        let code = '';
        for (let i = 0; i < codeLength; i++) {
            let rand = Math.floor (Math.random () * codeString.length);
            code += codeString.charAt (rand);
        }
        this.code = code;
    }

    
}