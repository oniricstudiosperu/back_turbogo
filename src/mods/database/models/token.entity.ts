import { Entity, Column, PrimaryGeneratedColumn, Index, OneToMany, ManyToOne, BaseEntity } from 'typeorm';
import { Role } from './auth/role.entity';
import { User } from './user.entity';
import { IOperation } from './ioperation';
const bcrypt = require ('bcrypt');

@Entity('token')
export class Token extends IOperation {
  
    @PrimaryGeneratedColumn()
    id: number;
    
}