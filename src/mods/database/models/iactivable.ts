import { BaseEntity, Column } from "typeorm";

export class IActivable extends BaseEntity {
    
    @Column()
    is_active:boolean;

    isActive():boolean {
        return this.is_active;
    }

    public static active(entity:IActivable) {
        if (entity) {
            return entity.isActive();
        } else {
            return false;
        }
        
    }
    
}