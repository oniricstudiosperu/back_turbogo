import { Entity, Column, PrimaryGeneratedColumn, Index, BaseEntity } from 'typeorm';

export enum MediaType {
    Image = 'image',
    Video = 'video'
};
@Entity('media')
export class Media extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 1500 })
    description: string;

    @Column({ length: 500 })
    path: string;

    @Column ()
    is_used:boolean;

    @Column ()
    creation_date:Date;

    @Column ({
        type: 'enum',
        enum: MediaType
    })
    type:MediaType;

}