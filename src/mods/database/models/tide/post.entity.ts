import { Entity, PrimaryGeneratedColumn, BaseEntity, OneToMany, Column, ManyToOne } from 'typeorm';
import { Album } from './album.entity';
import { Media } from '../media/media.entity';

@Entity('tide_post')
export class Post extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({ length: 1000 })
    description:string;
    
    @Column()
    creation_date:Date;

    @Column()
    updating_date:Date;

    @Column()
    is_used:boolean;

    @Column({ nullable: false })
    readonly albumId:number;

    @Column({ nullable: true })
    readonly mediaId:number;

    @ManyToOne (
        type => Album,
        album => album.posts,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    album:Album;
    
    @ManyToOne(
        type => Media,
        {
            nullable: true,
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE'
        }
    )
    media:Media;
}