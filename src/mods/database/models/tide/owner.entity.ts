import { Entity, PrimaryGeneratedColumn, BaseEntity, OneToMany, ManyToOne, JoinColumn, Column } from 'typeorm';
import { Album } from './album.entity';
import { Post } from './post.entity';

@Entity('tide_owner')
export class Owner extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;
    
    @OneToMany(
        type => Album,
        album => album.owner
    )
    albums:Album[];

    @Column({ nullable: true })
    readonly profileId:number;

    @Column({ nullable: true })
    readonly coverId:number;

    @ManyToOne(
        type => Post,
        {
            nullable: true,
            onDelete: 'SET NULL',
            onUpdate: 'SET NULL'
        }
    )
    @JoinColumn ({
        name: 'profileId'
    })
    profile:Post;

    @ManyToOne(
        type => Post,
        {
            nullable: true,
            onDelete: 'SET NULL',
            onUpdate: 'SET NULL'
        }
    )
    @JoinColumn ({
        name: 'coverId'
    })
    cover:Post;
    
    public static async createOne () {
        let owner = new Owner();
        owner.profile = null;
        return await owner.save();
    }
}