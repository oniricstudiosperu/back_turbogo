import { Entity, Column, PrimaryGeneratedColumn, Index, BaseEntity, ManyToOne, OneToMany } from 'typeorm';
import { Owner } from './owner.entity';
import { Post } from './post.entity';

@Entity('tide_album')
export class Album extends BaseEntity {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    name: string;

    @Column({ length: 500 })
    description: string;

    @Column({ nullable: false })
    ownerId:number;

    @ManyToOne (
        type => Owner,
        owner => owner.albums,
        {
            nullable: false,
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE'
        }
    )
    owner:Owner;

    @OneToMany(
        type => Post,
        post => post.album
    )
    posts:Post[];

}