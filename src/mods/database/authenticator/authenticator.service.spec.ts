import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticatorService } from './authenticator.service';
import { DatabaseModule } from 'src/mods/database/database.module';
import { SettingsService } from '../prov/settings/settings.service';

describe('AuthenticatorService', () => {
  let service: AuthenticatorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
      providers: [SettingsService, AuthenticatorService]
    }).compile();

    service = module.get<AuthenticatorService>(AuthenticatorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
