import { Injectable } from '@nestjs/common';
import { User } from 'src/mods/database/models/user.entity';
import { Role } from 'src/mods/database/models/auth/role.entity';
import { Token } from 'src/mods/database/models/token.entity';
import { SettingsService } from 'src/mods/database/prov/settings/settings.service';
import { Wasting } from 'src/classes/wasting.class';
import { Permission } from 'src/mods/database/models/auth/permission.entity';
import { IOperation } from '../models/ioperation';
import { Auth } from '../auth.class.ts/auth.class';
const jwt = require ('jsonwebtoken');

export enum AuthStatus {
    Unactive = 'unactive',
    Empty = 'empty',
    Failed = 'failed',
    OutOfTime = 'outoftime',
    NotValidated = 'notvalidated',
    Success = 'success',
}

@Injectable()
export class AuthenticatorService {
    
    constructor (
        private settings:SettingsService
    ) {
    }

    async create (req) {
        let auth = new Auth(this.settings);
        await auth.auth (req);
        return auth;
    }

}
