import { Injectable } from '@nestjs/common';
import { User } from 'src/mods/database/models/user.entity';
import { Role } from 'src/mods/database/models/auth/role.entity';
import { Token } from 'src/mods/database/models/token.entity';
import { SettingsService } from 'src/mods/database/prov/settings/settings.service';
import { Wasting } from 'src/classes/wasting.class';
import { Permission } from 'src/mods/database/models/auth/permission.entity';
import { IOperation } from '../models/ioperation';
const jwt = require ('jsonwebtoken');

export enum AuthStatus {
    Unactive = 'unactive',
    Empty = 'empty',
    Failed = 'failed',
    OutOfTime = 'outoftime',
    NotValidated = 'notvalidated',
    Success = 'success',
}

export class Auth {

    user:User;
    role:Role;
    permissions:Permission[];
    session:Token;
    status:AuthStatus;
    auths:number = 0;

    constructor (
        private settings:SettingsService
    ) { }

    async createToken (user:User):Promise<string> {
        
        if (!user) {
            throw { error: 'The user in create user have to be defined' };
        }

        let token = new Token();
        token.expiry_date = new Date(Date.now() + this.settings.sessionExpireTime * 1000);
        token.creation_date = new Date(Date.now() - 1000);
        token.is_active = true;
        token.user = user;
        
        await token.save();

        let turn = await jwt.sign ({
            uid: user.id,
            token: token.id
        }, this.settings.jwtKey);
        
        return turn;
    }

    protected allNull() {
        this.user = null;
        this.role = null;
        this.session = null;
    }
    
    protected setUnactive() {
        this.allNull();
        this.status = AuthStatus.Unactive;
    }

    protected setEmpty () {
        this.allNull();
        this.status = AuthStatus.Empty;
    }
    protected setFailed() {
        this.allNull();
        this.status = AuthStatus.Failed;
    }
    protected setOutOfTime () {
        this.allNull();
        this.status = AuthStatus.OutOfTime;
    }
    protected setNotValidated () {
        this.status = AuthStatus.NotValidated;
        return true;
    }
    
    /**
     * auth
     */
    public async auth(req) {
        this.auths++;
        
        this.setEmpty();

        if (!req.headers.access_token) {
            console.log ('fail on auth, access_token not present in header');
            return this.setFailed();
        }

        if (!req.headers.uid) {
            console.log ('fail on auth, uid not present in header. Received: ');
            return this.setFailed();
        }
        
        try {
            let accessToken = jwt.verify (req.headers.access_token, this.settings.jwtKey);
            
            if (!accessToken.uid
                || accessToken.uid != req.headers.uid
                || !accessToken.token) {
                console.log ('fail on auth, uid or token not present in data or uid != uid');
                return this.setFailed();
            }

            let token = await this.findToken (accessToken.token);
            this.status = AuthStatus.Success;
            if (this.inspectToken (token) !== true) {
                return;
            }

            if (this.status == AuthStatus.Success || this.status == AuthStatus.NotValidated) {
                token.expiry_date = new Date(Date.now() + this.settings.sessionExpireTime * 1000);
                await token.save();
            }

            this.user = await this.findUser(token);
            if (!this.user) {
                
                throw { message: '' };
            }
            this.role = this.user.role;
            this.permissions = this.user.role.permissions;
            
            
        } catch (ex) {
            this.setFailed();
        }
        return;
    }

    async findToken (id) {
        return await Token.findOne ({
            where: {
                id
            },
            relations: [ 'user' ]
        })
    }

    inspectToken (token:Token) {
        if (!token) {
            return this.setFailed();
        }

        switch (token.test()) {
            case IOperation.OutOfDate:
                return this.setOutOfTime();
            case IOperation.Unactive:
                return this.setUnactive();
            default:
                if (!token.user.is_email_valid && !token.user.is_phone_valid) {
                    return this.setNotValidated();
                }
                return true;
        }
    }

    async findUser (token:Token) {
        return await User.findOne ({
            where: { id: token.user.id },
            relations: ['role', 'role.permissions']
        });
        
    }

    public hasUser (includeNotValidated?:boolean) {
        if (!(this.status == AuthStatus.Success ||
            (!!includeNotValidated && this.status == AuthStatus.NotValidated))) {
            return false;
        }
        return true;
    }

    public requireUser (includeNotValidated?:boolean) {
        if (this.status != AuthStatus.Success) {
            if (this.status == AuthStatus.NotValidated) {
                if (!includeNotValidated) {
                    Wasting.throwForbidden();
                }
                
                return true;
            } else {
                Wasting.throwNotLogged();
            }
        } else {
            
            return true;
        }
    }

    public requireRol (rolName:string|Array<string>) {
        this.requireUser();
        if (!(rolName instanceof Array)) {
            rolName = [rolName];
        }
        
        let flag:boolean = false;
        for (let i in rolName) {
            if (this.role.name == rolName[i]) {
                flag = true;
            }
        }
        if (!flag) {
            Wasting.throwForbidden();
        }
    }
    public requirePermission (permissionName:string | Array<string>) {
        this.requireUser();
        if (!(permissionName instanceof Array)) {
            permissionName = [permissionName];
        }
        let flag:boolean = false;
        for (let i in permissionName) {
            for (let j in this.permissions) {
                if (this.permissions[j].name == permissionName[i]) {
                    flag = true;
                    break;
                }
            }
            if (flag) {
                break;
            }
        }
        if (!flag) {
            Wasting.throwForbidden();
        }
    }
    
    public async switchRole (rolName:string|Array<string>, ...prom:Array<Function>) {
        rolName = rolName instanceof Array ? rolName : [rolName];
        
        if (this.status != AuthStatus.Success) {
            return null;
        }

        let flag:boolean = false;
        for (let i in rolName) {
            if (this.role.name == rolName[i]) {
                flag = true;
                if (prom[i]) {
                    prom[i] ();
                }
            }
        }
        if (!flag) {
            return null;
        }

    }
    /*public handleException (e:any) {
        if (e instanceof RecoverableException) {
            
        } else {
            throw e;
        }
    }*/
}
