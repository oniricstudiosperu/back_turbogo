import { Module } from '@nestjs/common';
import { settingsProviders } from './prov/settings.providers';
import { AuthenticatorService } from './authenticator/authenticator.service';
import { SettingsService } from './prov/settings/settings.service';

@Module({
    exports: [
        ...settingsProviders,
        AuthenticatorService,
        SettingsService
    ],
    providers: [
        ...settingsProviders,
        AuthenticatorService,
        SettingsService
    ]
})
export class DatabaseModule {}

