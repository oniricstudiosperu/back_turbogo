import { Test, TestingModule } from '@nestjs/testing';
import { SettingsService } from './settings.service';

describe('SettingsService', () => {
  let service: SettingsService;

  beforeEach(async () => {
    try {
      const module: TestingModule = await Test.createTestingModule({
        providers: [SettingsService],
      }).compile();
  
      service = module.get<SettingsService>(SettingsService);
    } catch (e) {
      console.log (e);
    }
    
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
