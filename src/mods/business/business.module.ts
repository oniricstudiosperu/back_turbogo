import { Module } from '@nestjs/common';
import { EnterprisesController } from './enterprises/enterprises.controller';
import { DatabaseModule } from '../database/database.module';
import { UserEnterprisesController } from './user-enterprises/user-enterprises.controller';
import { RolesController } from './roles/roles.controller';

@Module({
  imports: [DatabaseModule],
  controllers: [EnterprisesController, UserEnterprisesController, RolesController]
})
export class BusinessModule {}
