import { Controller, Get, Query } from '@nestjs/common';
import { Structure } from 'src/classes/Structure.class';
import { EnterpriseRole } from 'src/mods/database/models/bus/enterprise_role.entity';

@Controller('enterprise-roles')
export class RolesController {

    constructor(

    ) { }

    @Get()
    async _get(@Query() query) {
        return await Structure.newList (query.page, async () => {
            return await EnterpriseRole.find();;
        });
    }
}
