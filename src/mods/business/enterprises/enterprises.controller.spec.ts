import { Test, TestingModule } from '@nestjs/testing';
import { EnterprisesController } from './enterprises.controller';
import { DatabaseModule } from 'src/mods/database/database.module';

describe('Enterprises Controller', () => {
  let controller: EnterprisesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
      controllers: [EnterprisesController],
    }).compile();

    controller = module.get<EnterprisesController>(EnterprisesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
