import { Controller, Get, Param, NotFoundException, Put, ForbiddenException, Body, Delete, Query, Req } from '@nestjs/common';
import { AuthenticatorService } from 'src/mods/database/authenticator/authenticator.service';
import { Enterprise } from 'src/mods/database/models/bus/enterprise.entity';
import { EnterprisePermission } from 'src/mods/database/models/bus/enterprise_permission.entity';
import { Input, Probe } from 'src/classes/Probe.class';
import { Media } from 'src/mods/database/models/media/media.entity';
import { Post } from 'src/mods/database/models/tide/post.entity';
import { Delimit } from 'src/classes/delimit.class';
import { BaseEntity } from 'typeorm';
import { EnterpriseRoleUser } from 'src/mods/database/models/bus/enterprise_role_user.entity';
import { Owner } from 'src/mods/database/models/tide/owner.entity';
import { User } from 'src/mods/database/models/user.entity';

@Controller('enterprises')
export class EnterprisesController {
    
    constructor (
    ) { }

    async getEnterprise(id, req, upper?:any):Promise<Enterprise> {
        let enterprise = await req.auth.user.getEnterprise(id);
        if (!enterprise) {
            throw new NotFoundException();
        }
        
        let list = req.auth.user.hideEnterprises (enterprise);
        if (list.length == 0) { throw new NotFoundException(); }

        if (upper && upper.members) {
            enterprise.enterpriseRoleUsers = await EnterpriseRoleUser.find ({
                where: { enterprise },
                relations: ['role', 'user', 'user.tide_owner']
            });
            for (let i in enterprise.enterpriseRoleUsers) {
                let rel = enterprise.enterpriseRoleUsers[i];
                delete rel.user.password;
            }
        }
        
        return enterprise;
    }

    @Get(':id') 
    async _get_id (@Param('id') id, @Query('support') support:string, @Req() req):Promise<Enterprise> {
        req.auth.requireUser();
        let upper = [];
        if (support) {
            let supported = support.split (',');
            for (let i in supported) {
                upper[supported[i].trim()] = true;
            }
        }
        let enterprise = await this.getEnterprise(id, req, upper);
        return enterprise;
    }

    async Model (input, body, key, model:any, message:string) {
        if (typeof (body[key]) != 'undefined') {
            let mo = await model.findOne (body[key]);
            if (!mo) {
                input.throwError (key, message);
            }
            return mo;
        }
        return undefined;
    }

    @Put(':id')
    async _put_id (@Param('id') id, @Body() body, req):Promise<Enterprise> {
        req.auth.requireUser();
        let enterprise = await this.getEnterprise (id, req);
        
        let permissions = await EnterprisePermission.selectByRole (enterprise.enterpriseRoleUsers[0].roleId);
        
        if (!EnterprisePermission.isIn('update_info', permissions)) {
            throw new ForbiddenException();
        }
        
        let input = new Input(body);
        input.probe ('name', Probe.String ({
            minLength: 1,
            maxLength: 250,
            skipUndefined: true
        }));
        input.probe ('description', Probe.String ({
            minLength: 1,
            maxLength: 500,
            skipUndefined: true
        }));
        input.probe ('profile', Probe.String({
            minLength: 1,
            skipUndefined: true
        }));
        input.probe ('cover', Probe.String({
            minLength: 1,
            skipUndefined: true
        }));

        let profile = await this.Model (input, body, 'profile', Post, 'No se encontró el post de perfil')
        let cover = await this.Model (input, body, 'cover', Post, 'No se encontró el post de fondo');
        
        Delimit.clone (enterprise, body, 'name', 'description');
        if (profile) {
            enterprise.tide_owner.profile = profile;
        }
        if (cover) {
            enterprise.tide_owner.cover = cover;
        }
        await enterprise.tide_owner.save();
        return enterprise;
    }

    @Delete(':id')
    async _delete_id (@Param('id') id, @Body() body, @Req() req) {
        req.auth.requireUser();
        let enterprise = await this.getEnterprise (id, req);
        
        let permissions = await EnterprisePermission.selectByRole (enterprise.enterpriseRoleUsers[0].roleId);
        
        if (!EnterprisePermission.isIn('delete', permissions)) {
            throw new ForbiddenException();
        }
        let entId = enterprise;
        await Owner.delete ({ id: enterprise.tideOwnerId });
        await enterprise.remove();
        return {
            success: true,
            id: entId
        };
    }

}
