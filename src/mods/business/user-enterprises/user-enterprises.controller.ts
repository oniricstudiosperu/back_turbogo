import { Controller, Get, Param, Query, NotFoundException, Body, Post, UnauthorizedException, ForbiddenException, Req } from '@nestjs/common';
import { AuthenticatorService } from 'src/mods/database/authenticator/authenticator.service';
import { Structure } from 'src/classes/Structure.class';
import { Input, Probe } from 'src/classes/Probe.class';
import { User } from 'src/mods/database/models/user.entity';
import { Enterprise } from 'src/mods/database/models/bus/enterprise.entity';
import { EnterpriseRole } from 'src/mods/database/models/bus/enterprise_role.entity';
import { EnterpriseRoleUser } from 'src/mods/database/models/bus/enterprise_role_user.entity';
import { Owner } from 'src/mods/database/models/tide/owner.entity';

@Controller('user/:userId/enterprises')
export class UserEnterprisesController {

    constructor (
    ) { }

    async getUser(userId):Promise<User> {
        let user = await User.findOne (userId);
        if (!User.active (user)) { throw new NotFoundException (); }
        return user;
    }
    
    @Get()
    async _get (@Param ('userId') userId, @Query('page') page, @Req() req) {
        req.auth.requireUser();
        let user = await this.getUser (userId);
        return await Structure.newList (page, async (limit, skip) => {
            return await user.getEnterpriseList(req.auth.user);
        });
    }

    @Post()
    async _post(@Param('userId') userId, @Body() body, @Req() req) {
        req.auth.requirePermission('enterprise_create');
        if (userId != req.auth.user.id) {
            throw new ForbiddenException();
        }
        let input = new Input(body);
        input.probe ('name', Probe.String ({
            minLength: 1,
            maxLength: 250
        }));
        input.probe ('description', Probe.String ({
            minLength: 1,
            maxLength: 500
        }));

        let enterprise = await this.createEnterprise (req.auth.user, body)
        
        return {
            success: true,
            id: enterprise.id
        }
    }

    async createEnterprise (user:User, body: {
        name:string,
        description:string
    }) {
        let enterprise = new Enterprise();
        enterprise.name = body.name;
        enterprise.description = body.description;
        enterprise.creation_date = new Date();
        enterprise.is_visible = true;
        enterprise.tide_owner = await Owner.createOne();
        
        let result = await enterprise.save();

        let role = await EnterpriseRole.findSuper();
        
        let rel = new EnterpriseRoleUser();
        rel.enterprise = result;
        rel.user = user;
        rel.role = role;
        rel.is_active = true;
        await rel.save();
        return enterprise;
    }
}
