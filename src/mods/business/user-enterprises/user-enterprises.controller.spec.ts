import { Test, TestingModule } from '@nestjs/testing';
import { UserEnterprisesController } from './user-enterprises.controller';
import { User } from 'src/mods/database/models/user.entity';
import { DatabaseModule } from 'src/mods/database/database.module';

describe('UserEnterprises Controller', () => {
  let controller: UserEnterprisesController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
      controllers: [UserEnterprisesController],
    }).compile();

    controller = module.get<UserEnterprisesController>(UserEnterprisesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  
  it ('should should create an enterprise', async () => {
    let user = new User ();
    user.id = 1;

    let enterprise = await controller.createEnterprise (user, {
      name: 'test',
      description: 'test'
    });

    console.log (enterprise);

    //controller._post (84, 1)
  });

});
