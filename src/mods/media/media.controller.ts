import { Controller, Post, UseInterceptors, UploadedFile } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('media')
export class MediaController {

    @Post()
    @UseInterceptors(
        FileInterceptor('file')
    )
    async _post (@UploadedFile() file) {
        console.log (file);
    }

}
