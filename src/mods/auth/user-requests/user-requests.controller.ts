import { Controller, Get, Param, Req, Post, ForbiddenException, NotFoundException, Delete } from '@nestjs/common';
import { User } from 'src/mods/database/models/user.entity';
import { Request, ShipRequestType } from 'src/mods/database/models/ship/request.entity';
import { Auth } from 'src/mods/database/auth.class.ts/auth.class';
import { Structure } from 'src/classes/Structure.class';
import { userInfo } from 'os';

@Controller('user/:id/requests')
export class UserRequestsController {

    constructor (

    ) {}

    @Get(':userId')
    async _get (@Param('id') id, @Param('userId') userId, @Req() req) {
        req.auth.requireUser();

        let user = await User.getRepository()
        .createQueryBuilder('user')
        .leftJoinAndSelect ('user.createdRequest', 'ship', 'ship.id=:userId', { userId })
        .where ('user.id=:id', { id })
        .getOne();

        let request = await Request.getRepository()
            .createQueryBuilder ('request')
            .where ('request.creationUserId=:id', { id })
            .andWhere ('request.targetId=:userId', { userId })
            .andWhere ('request.is_active=:active', { active: true })
            .getOne();

        return request;
    }

    @Post (':userId')
    async _post (@Param ('id') id, @Param('userId') userId, @Req() req) {
        let auth:Auth = req.auth;
        auth.requireUser();

        if (auth.user.id != id) {
            throw new ForbiddenException();
        }

        let target = await User.findOne (userId);
        if (!target) {
            throw new NotFoundException();
        }

        let ship = await auth.user.findShip (target);
        if (ship.ships.length) {
            return {
                success: false,
                reason: 'Ya existe una amistad'
            };
        }

        let request = await Request.getRepository()
            .createQueryBuilder ('request')
            .where ('request.creationUserId=:id', { id })
            .andWhere ('request.targetId=:userId', { userId })
            .andWhere ('request.is_active=:active', { active: true })
            .getOne();

        if (request) {
            return {
                success: false,
                reason: 'Ya existe una petición',
                data: request
            };
        }
        let requests = [new Request(), new Request()];
        requests[0].creation_user = requests[1].target = auth.user;
        requests[0].target = requests[1].creation_user = target;
        requests[0].creation_date = requests[1].creation_date = new Date();
        requests[0].type = ShipRequestType.Requester;
        requests[1].type = ShipRequestType.Requesting;
        requests[0].is_active = requests[1].is_active = true;

        await requests[0].save();
        await requests[1].save();

        return {
            success: true,
            data: requests[0]
        };

    }

    @Delete (':userId/:requestId')
    async _delete (
        @Param ('id') id,
        @Param('userId') userId,
        @Req() req,
        @Param ('requestId') reqId
    ) {
        let auth:Auth = req.auth;
        auth.requireUser();

        if (auth.user.id != id) throw new ForbiddenException();

        let target = await User.findOne(userId);
        if (!target) throw new NotFoundException();

        if (!target.isActive()) throw new NotFoundException();

        let r = await Request.findOne(reqId);
        if (!r) throw new NotFoundException();
        if (!r.isActive()) throw new NotFoundException();
        
        if (!((r.creationUserId == id && r.targetId == userId) ||
            (r.creationUserId == userId && r.targetId == id))) {
            throw new ForbiddenException();
        }

        await Request.update ({
            creationUserId: id,
            targetId: userId
        }, {
            is_active: false
        });

        await Request.update ({
            creationUserId: userId,
            targetId: id
        },{
            is_active: false
        });

        return {
            success: true
        }
    }
    
}
