import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { MenuController } from './menu/menu.controller';
import { UserController } from './user/user.controller';
import { SenderModule } from '../sender/sender.module';
import { UserShipsController } from './user-ships/user-ships.controller';
import { UserRequestsController } from './user-requests/user-requests.controller';

@Module({
    imports: [DatabaseModule, SenderModule],
    controllers: [MenuController, UserController, UserShipsController, UserRequestsController]
})
export class AuthModule {}
