import { Controller, Get, Param, Post, Req, Delete, ForbiddenException, NotFoundException, Query } from '@nestjs/common';
import { AuthenticatorService } from 'src/mods/database/authenticator/authenticator.service';
import { User } from 'src/mods/database/models/user.entity';
import { Request, ShipRequestType } from 'src/mods/database/models/ship/request.entity';
import { Auth } from 'src/mods/database/auth.class.ts/auth.class';
import { Structure } from 'src/classes/Structure.class';

@Controller('user/:id/ships')
export class UserShipsController {

    constructor (
    ) { }

    @Get()
    async _get (
        @Query() query,
        @Req() req,
        @Param('id') id
    ) {
        let auth:Auth = req.auth;
        req.auth.requireUser();
        if (id != auth.user.id) {
            throw new ForbiddenException();
        }
        return await Structure.newList (query.page, async (limit, skip) => {
            let where = 'user.id=:uid';
            let whereData:any = { uid: id };
            if (query.q) {
                where += ' and (ship.firstname like :q or ship.lastname like :q)';
                whereData.q = '%' + query.q + '%'
            }
            let users = await User.getRepository().createQueryBuilder('user')
                .leftJoinAndSelect ('user.ships', 'ship')
                .leftJoinAndSelect ('ship.tide_owner', 'ship_owner')
                .where (where, whereData)
                .limit (limit)
                .skip (skip)
                .getMany();
            if (users.length) {
                for (let i in users[0].ships) {
                    delete users[0].ships[i].password
                }
                return users[0].ships;
            } else {
                return [];
            }
        });
    }

    @Get(':userId')
    async _get_id (@Param('id') id, @Param('userId') userId, @Req() req) {
        req.auth.requireUser();

        let user = await User.getRepository()
        .createQueryBuilder('user')
        .leftJoinAndSelect ('user.ships', 'ship', 'ship.id=:userId', { userId })
        .where ('user.id=:id', { id })
        .getOne();

        return user.ships;
    }

    @Post (':userId/:requestId')
    async _post (
        @Param ('id') id,
        @Param ('userId') userId,
        @Req () req,
        @Param ('requestId') reqId
    ) {
        let auth:Auth = req.auth;
        auth.requireUser();

        if (auth.user.id != id) throw new ForbiddenException();

        let target = await User.findOne(userId);
        if (!target) throw new NotFoundException();

        if (!target.isActive()) throw new NotFoundException();

        let r = await Request.findOne({
            where: { id: reqId }, relations: ['target', 'creation_user'] });
        if (!r) throw new NotFoundException();
        if (!r.isActive()) throw new NotFoundException();
        
        if (!(r.creationUserId == id && r.targetId == userId)) {
            throw new NotFoundException();
        }

        if (r.type != ShipRequestType.Requesting) {
            throw new NotFoundException();
        }
        
        let ship = await r.generateShip ();

        await Request.update (
            { creationUserId: id, targetId: userId },
            { is_active: false }
        );

        await Request.update (
            { creationUserId: userId, targetId: id },
            { is_active: false }
        );

        return {
            success: true,
            data: r.target
        };
    }

    
}
