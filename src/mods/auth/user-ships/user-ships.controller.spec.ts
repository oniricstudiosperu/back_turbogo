import { Test, TestingModule } from '@nestjs/testing';
import { UserShipsController } from './user-ships.controller';
import { DatabaseModule } from 'src/mods/database/database.module';

describe('UserShips Controller', () => {
  let controller: UserShipsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
      controllers: [UserShipsController]
    }).compile();

    controller = module.get<UserShipsController>(UserShipsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
