import { Controller, Get, Request, Req } from '@nestjs/common';
import { AuthenticatorService } from 'src/mods/database/authenticator/authenticator.service';

@Controller('menu')
export class MenuController {

    constructor (
    ) { }

    @Get ()
    async _get(@Req() req) {
        if (req.auth.hasUser()) {
            return await req.auth.user.getMenu()
        } else {
            return [];
        }
    }

}
