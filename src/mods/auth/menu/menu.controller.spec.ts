import { Test, TestingModule } from '@nestjs/testing';
import { MenuController } from './menu.controller';
import { DatabaseModule } from 'src/mods/database/database.module';

describe('Menu Controller', () => {
  let controller: MenuController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MenuController],
      imports: [DatabaseModule] 
    }).compile();

    controller = module.get<MenuController>(MenuController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be defined', async () => {
    
    expect (await controller._get ()).toMatchObject ([]);
  });


});
