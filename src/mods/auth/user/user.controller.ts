import { Controller, Get, BadRequestException, Param, Put, NotFoundException, Body, Post, Query, Req } from '@nestjs/common';
import { AuthenticatorService, AuthStatus } from 'src/mods/database/authenticator/authenticator.service';
import { User } from 'src/mods/database/models/user.entity';
import { Validate, ValidateTarget } from 'src/mods/database/models/op/validate.entity';
import { Input, Probe } from 'src/classes/Probe.class';
import { SettingsService } from 'src/mods/database/prov/settings/settings.service';
import { identity } from 'rxjs';
import { MailSenderService } from 'src/mods/sender/mail-sender/mail-sender.service';
import { Structure } from 'src/classes/Structure.class';
import { async } from 'rxjs/internal/scheduler/async';
import { Auth } from 'src/mods/database/auth.class.ts/auth.class';

@Controller('user')
export class UserController {

    constructor (
        private settings:SettingsService,
        private mail:MailSenderService
    ) { }
    
    @Get()
    async _get (
        @Query() query,
        @Req() req
    ) {
        let auth:Auth = req.auth;
        req.auth.requireUser();
        return await Structure.newList (query.page, async (limit, skip) => {
            let where = '';
            let whereData:any = {};
            if (query.q) {
                where = 'user.firstname like :q or user.lastname like :q';
                whereData.q = '%' + query.q + '%';
            }
            let users = await User.findByQuery (where, whereData)
                .leftJoinAndSelect ('user.ships', 'ship', 'ship.id=:shipId', { shipId: auth.user.id })
                .limit (limit)
                .skip (skip)
                .getMany();
            for (let i in users) {
                delete users[i].password
            }
            return users;
        });
    }

    @Get ('me')
    async _get_me (@Req() req) {
        req.auth.requireUser(true);
        return await this._get_id (req.auth.user.id, req);
    }

    @Get (':id')
    async _get_id (@Param('id') id, @Req() req) {
        req.auth.requireUser (true);
        if (req.auth.status == AuthStatus.NotValidated) {
            let validation = await req.auth.user.getValidation(this.settings);
            delete validation.code;
            throw new BadRequestException ({
                error: 'Not Validated',
                code: 2001,
                operation: validation
            });
        }

        let user = await User.findOneBasic (id);
        
        return user;
    }

    @Post ('me/validate/:idOperation')
    async _put_me_validate (@Param('idOperation') idOperation, @Body() body, @Req() req) {
        req.auth.requireUser (true);
        return await this._put_operation (req.auth.user.id, idOperation, body, req);
    }

    @Post ('me/validate/:idOperation/resend')
    async _post_me_validate_resend (@Param ('idOperation') idOperation, @Req() req) {
        req.auth.requireUser (true);
        
        let user = req.auth.user;
        
        let validate = await Validate.findOne (idOperation);
        if (!validate || validate.test() != Validate.Active) {
            throw new NotFoundException();
        }
        if (validate.target == ValidateTarget.Email) {
            await this.mail.sendFromServer (user.email, MailSenderService.tempValidate, { code: validate.code });
        }
        return {
            success: true
        }
    }

    @Post (':id/validate/:idOperation')
    async _put_operation (@Param('id') id, @Param('idOperation') idOperation, @Body() body, @Req() req) {
        req.auth.requireUser (true);
        
        if (req.auth.user.id !== id) {
            console.log ('userid ' + req.auth.user.id + ' != ' + id);
            throw new NotFoundException();
        }
        let validate = await Validate.findOne ({
            id: idOperation,
            user: req.auth.user
        });
        validate.user = req.auth.user;

        if (!validate || validate.test() != Validate.Active) {
            throw new NotFoundException();
        }
        let input = new Input(body);
        
        input.probe ('code', Probe.String ({
            minLength: 1,
            maxLength: 6
        }));

        if (validate.code != body.code) {
            input.throwError ('code', 'El código no es el mismo');
        }

        validate.is_active = false;
        if (validate.target == ValidateTarget.Email) {
            req.auth.user.is_email_valid = true;
        } else if (validate.target == ValidateTarget.Phone) {
            req.auth.user.is_phone_valid = true;
        }
        await validate.save();
        await req.auth.user.save();
        return {
            success: true
        };
    }

}
