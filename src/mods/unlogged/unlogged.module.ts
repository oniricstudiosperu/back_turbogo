import { Module } from '@nestjs/common';
import { RegisterController } from './co/register/register.controller';
import { DatabaseModule } from '../database/database.module';
import { SenderModule } from '../sender/sender.module';
import { SessionController } from './co/session/session.controller';

@Module({
  controllers: [RegisterController, SessionController],
  imports: [DatabaseModule, SenderModule]
})
export class UnloggedModule {}
