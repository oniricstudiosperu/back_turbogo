import { Test, TestingModule } from '@nestjs/testing';
import { RegisterController } from './register.controller';
import { DatabaseModule } from 'src/mods/database/database.module';
import { SenderModule } from 'src/mods/sender/sender.module';

describe('Register Controller', () => {
  let controller: RegisterController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RegisterController],
      imports: [DatabaseModule, SenderModule]
    }).compile();

    controller = module.get<RegisterController>(RegisterController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be defined', async () => {
    try {
      await controller._post ({
        firstname: 'Jesús',
        lastname: 'Curi',
        account: 'jesuscuri13@gmail.com',
        password: 'Congestion306',
        birthdate: new Date().toString()
      })
    } catch (e) {
      //console.log (e);
      expect (e.response.error.account).toBe ('El email ya existe');
      //throw e;
    }
    
  });
});
