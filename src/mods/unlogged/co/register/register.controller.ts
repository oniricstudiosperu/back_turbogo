import { Controller, Post, Body, Req } from '@nestjs/common';
import { User } from 'src/mods/database/models/user.entity';
import { RegisterDto } from '../../cl/register-dto.class';
import { Probe, Input } from 'src/classes/Probe.class';
import { Role } from 'src/mods/database/models/auth/role.entity';
import { AuthenticatorService } from 'src/mods/database/authenticator/authenticator.service';
import { SettingsService } from 'src/mods/database/prov/settings/settings.service';
import { ValidateTarget } from 'src/mods/database/models/op/validate.entity';
import { Mail } from 'src/mods/database/models/mail.entity';
import { MailSenderService } from 'src/mods/sender/mail-sender/mail-sender.service';
import { Owner } from 'src/mods/database/models/tide/owner.entity';

@Controller('register')
export class RegisterController {

    constructor (
        private settings:SettingsService,
        private mail:MailSenderService,
    ) { }

    /**
     * For creating a new registered user with a email or cellphone number
     * 
     * @param body an object that contains { 
     *  type:(phone, email)
     *  email
     *  password
     * }
     * 
     */
    @Post()
    async _post(@Body() body:RegisterDto, @Req() req) {
        let input = new Input(body);
        input.probe ('firstname', Probe.String ({
            minLength: 1,
            maxLength: 250
        }));
        input.probe ('lastname', Probe.String ({
            minLength: 1,
            maxLength: 250
        }));
        input.probe ('account', Probe.String ());
        
        input.probe ('password', Probe.String ());

        input.probe ('birthdate', Probe.Date ());

        let user = new User();
        /* generate account */
        
        user.account = '';
        user.creation_date = new Date(Date.now());
        user.email = body.account;
        user.firstname = body.firstname;
        
        user.is_active = true;
        user.is_email_valid = false;
        user.is_phone_valid = false;
        user.lastname = body.lastname;
        user.phone = null;
        user.role = await Role.findOne (1);

        await user.setPassword (body.password);

        let users = await User.find ({
            email: body.account,
            is_active: true
        });
        for (let i in users) {
            if (users[i].is_email_valid) {
                input.throwError ('account', 'El email ya existe')
            } else {
                users[i].is_active = false;
                await users[i].save();
            }
        }
        user.tide_owner = await Owner.createOne();
        await user.save();
        let operation = await user.createValidate (this.settings, ValidateTarget.Email);
        if (operation.target == ValidateTarget.Email) {
            await this.mail.sendFromServer (user.email, MailSenderService.tempValidate, { code: operation.code });
        }
        delete operation.code;
        let access_token = await req.auth.createToken (user);

        return {
            uid: user.id,
            access_token: access_token,
            operation
        };
    }
}
