import { Test, TestingModule } from '@nestjs/testing';
import { SessionController } from './session.controller';
import { DatabaseModule } from 'src/mods/database/database.module';

describe('Session Controller', () => {
  let controller: SessionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
      controllers: [SessionController],
    }).compile();

    controller = module.get<SessionController>(SessionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
