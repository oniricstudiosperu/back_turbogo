import { Controller, Post, Body, Req } from '@nestjs/common';
import { AuthenticatorService } from 'src/mods/database/authenticator/authenticator.service';
import { Input, Probe } from 'src/classes/Probe.class';
import { User } from 'src/mods/database/models/user.entity';
import { Token } from 'src/mods/database/models/token.entity';

@Controller('session')
export class SessionController {

    constructor(
    ) { }

    @Post()
    async _post (@Body() body, @Req() req) {
        let input = new Input(body);
        input.probe ('account', Probe.String ({ minLength: 1 }));
        input.probe ('password', Probe.String ({ minLength: 1 }));
        
        let users = await User.findBy ('email=:account', { account: body.account });
        
        if (!users.length) {
            input.throwError ('account', 'La cuenta no existe');
        }
        if (users.length > 1) {
            throw 'An error ocurred, there are more than 1 user with the same account';
        }
        let user = users[0];
        
        if (!(await user.comparePassword (body.password))) {
            input.throwError ('password', 'La contraseña no coincide');
        }
        let token = await req.auth.createToken (user);
        return {
            access_token: token,
            uid: user.id
        };

    }

}
