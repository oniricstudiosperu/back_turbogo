export class RegisterDto {
    
    firstname:string;
    
    lastname:string;
    
    account:string;

    password:string;

    birthdate:string;
}