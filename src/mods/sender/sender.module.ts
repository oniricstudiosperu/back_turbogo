import { Module } from '@nestjs/common';
import { SmsSenderService } from './sms-sender/sms-sender.service';
import { MailSenderService } from './mail-sender/mail-sender.service';

@Module({
  exports: [SmsSenderService, MailSenderService],
  providers: [SmsSenderService, MailSenderService]
})
export class SenderModule {}
