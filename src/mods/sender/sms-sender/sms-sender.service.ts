import { Injectable } from '@nestjs/common';
import { Sms } from 'src/mods/database/models/sms.entity';
import { SmsSettings } from 'src/configurations/sms-settings';
@Injectable()
export class SmsSenderService {

    public async sendFromServer (to:string, template: { body:string }) {
        let sms = new Sms();
        sms.is_sent = false;
        sms.body = template.body;
        sms.from = SmsSettings.mainPhone;
        sms.body = template.body;
        sms.to = to;
        return await sms.save();
    }

}
