import { Injectable } from '@nestjs/common';
import { Mail } from 'src/mods/database/models/mail.entity';
import { MailSettings } from 'src/configurations/mail-settings';

@Injectable()
export class MailSenderService {
    
    public async sendFromServer (
        to:string,
        template: { body:string, subject:string },
        object:any
    ) {
        let mail = new Mail();
        mail.is_sent = false;
        mail.subject = template.subject;
        mail.body = template.body;
        for (let i in object) {
            mail.body = mail.body.replace ('{' + i + '}', object[i]);
        }
        
        mail.from = MailSettings.mainMail;
        mail.to = to;
        return await mail.save();
    }

    static tempValidate = {
        body: 
`Este correo es para validar su email en Oniric Studios.
Su código de validación es: {code}

Si no ha requerido este email, puede ignorarlo`,
        subject: `Validar correo`
    }
}
