import { HttpException, BadRequestException } from "@nestjs/common";

export function test(element, ...filters:Trying[]) {
    let thrown = {
        statusCode: 400,
        message: [],
        error: "Bad Request",
    };
    for (let i in filters) {
        try {
            filters[i].inspect(element);
        } catch (e) {
            if (e.error) {
                thrown.message.push (e.error);
            }
        }
    }
    if (thrown.message.length) {
        throw new HttpException(thrown, 400);
    }
}

export class Probe {
    static String (options?:StringOptions):Trying {
        return {
            inspect (element) {
                options = options || {}
                if (typeof(element) === 'undefined') {
                    if (options.skipUndefined) {
                        return element;
                    } else {
                        throw { error: 'Debe estar definido' };
                    }
                }
    
                if (element == null) {
                    if (options.nullable) {
                        return element;
                    } else {
                        throw { error: 'No puede ser nulo' };
                    }
                }
                
                if (options.minLength) {
                    if (element.length < options.minLength) {
                        throw { error: 'Debe tener por lo menos ' + options.minLength + ' caracteres' }
                    }
                }
                if (!isNaN(options.maxLength)) {
                    if (element.length > options.maxLength) {
                        throw { error: 'Debe tener como máximo ' + options.maxLength + ' caracteres'}
                    }
                }
                return element;
            }
        }
    }

    static Date (options?:Options):Trying {
        return {
            inspect (element) {
                options = options || {}
                
                if (typeof(element) === 'undefined') {
                    if (options.skipUndefined) {
                        return element;
                    } else {
                        throw { error: 'Debe estar definido' };
                    }
                }
                if (element == null) {
                    if (options.nullable) {
                        return element;
                    } else {
                        throw { error: 'No puede ser nulo' };
                    }
                }
                
                let dateTest = new Date(element);
                if (isNaN(dateTest.getTime())) {
                    throw { error: 'Debe ser una fecha válida' };
                }
                return dateTest;
            }
        }
    }

    static Email(options?:Options):Trying {
        return {
            inspect(element) {
                options = options || {}
                if (typeof(element) === 'undefined') {
                    if (options.skipUndefined) {
                        return element;
                    } else {
                        throw { error: 'Debe estar definido' };
                    }
                }
    
                if (element == null) {
                    if (options.nullable) {
                        return element;
                    } else {
                        throw { error: 'No puede ser nulo' };
                    }
                }
                if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(element))) {
                    throw { error: 'Debe tener formato de email' }
                }
                return element;
            }
        }
    }

    static Enum(type:{[key:string]:any}, options?:Options):Trying {
        return {
            inspect(element) {
                options = options || {}
                if (typeof(element) === 'undefined') {
                    if (options.skipUndefined) {
                        return element;
                    } else {
                        throw { error: 'Debe estar definido' };
                    }
                }
    
                if (element == null) {
                    if (options.nullable) {
                        return element;
                    } else {
                        throw { error: 'No puede ser nulo' };
                    }
                }
                if (typeof (type[element]) == 'undefined') {
                    throw { error: 'The value ' + element + ' is not allowed' }
                }
                return element;
            }
        }
    }
    static Password(options?:PasswordOptions):Trying {
        return {
            inspect(element) {
                if (typeof(element) === 'undefined') {
                    if (options.skipUndefined) {
                        return element;
                    } else {
                        throw { error: 'Debe estar definido' };
                    }
                }
                if (element == null) {
                    if (options.nullable) {
                        return element;
                    } else {
                        throw { error: 'No puede ser nulo' };
                    }
                }
                if (!(/.*[\d].*/).test (element)) {
                    throw { error: 'Debe tener por lo menos un número' };
                }
                if (!(/.*[a-z].*/).test (element)) {
                    throw { error: 'Debe tener por lo menos una letra minúscula' };
                }
                if (!(/.*[A-Z].*/).test (element)) {
                    throw { error: 'Debe tener por lo menos una letra mayúscula' };
                }
                if (!(/^.{8,20}$/).test (element)) {
                    throw { error: 'Debe tener de 8 a 20 caracteres' };
                }
                return element;
            }
        }
    }
}


export declare class Trying {
    inspect(element):any;
}


export class Options {
    skipUndefined?:boolean;
    nullable?:boolean;
}

export class StringOptions extends Options {
    minLength?:number;
    maxLength?:number;
}

export class PasswordOptions extends Options {
    /** Por defecto 8 */
    minLength?:number;
    /** Por defecto 20 */
    maxLength?:number;
}

export class Input {
    public error: { [key:string]:any };
    constructor (private body:{ [key:string]:any }) {
        this.error = {};
    }

    probe (index:string, funct:Trying) {
        this.error[index] = undefined;
        try {
            this.body[index] = funct.inspect (this.body[index])
        } catch (e) {
            this.error[index] = e.error;
            this.end();
        }
        
    }

    private end() {
        throw new BadRequestException({
            statusCode: 400,
            error: this.error
        });
    }

    throwError (index:string, message:string) {
        this.error[index] = message;
        this.end();
    }
}