export class Delimit {
    
    public static is(input, callback?:Function) {
        if (typeof(input) != 'undefined' && input !== null) {
            if (callback) {
                callback(input);
            }
            return true;
        }
        return false;
    }

    public static clone (to:any, from:any, ...keys:string[]) {
        for (let i in keys) {
            if (typeof (from[keys[i]]) != 'undefined') {
                to[keys[i]] = from[keys[i]];
            }
        }
    }
}