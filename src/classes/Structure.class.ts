export class Structure {

    static async newList (
        page:number,
        promise:(limit, skip) => Promise<any[]>) {
        try {
            let limit = 20;
            page = page || 1;
            let skip = (page - 1) * limit;
            
            let turn = {
                totalPage: limit,
                list: await promise (limit, skip)
            };
            return turn;
        } catch (e) {
            throw e;
        }
    }

}