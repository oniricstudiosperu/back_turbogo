import { BadRequestException, UnauthorizedException, ForbiddenException } from "@nestjs/common";

export class Wasting {
    static throwNotLogged () {
        throw new UnauthorizedException ({
            error: 'Usuario no logueado',
            code: 401
        })
    }

    static throwForbidden () {
        throw new ForbiddenException ({
            error: 'El usuario no tiene los permisos necesarios para realizar esta acción',
            code: 403
        })
    }
}