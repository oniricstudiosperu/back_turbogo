import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './mods/database/database.module';
import { UnloggedModule } from './mods/unlogged/unlogged.module';
import { AuthModule } from './mods/auth/auth.module';
import { AuthorizationMiddleware } from './middlewares/authorization.middleware';
import { SenderModule } from './mods/sender/sender.module';
import { BusinessModule } from './mods/business/business.module';
import { MediaModule } from './mods/media/media.module';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    DatabaseModule,
    UnloggedModule,
    AuthModule,
    SenderModule,
    BusinessModule,
    MediaModule,
    MulterModule.register ({
      dest: './.temp'
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthorizationMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
  
}
