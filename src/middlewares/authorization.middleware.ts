import { Injectable, NestMiddleware } from '@nestjs/common';
import { AuthenticatorService } from 'src/mods/database/authenticator/authenticator.service';


@Injectable()
export class AuthorizationMiddleware implements NestMiddleware {
  
  constructor (private auth:AuthenticatorService) { }
  
  async use(req: any, res: any, next: () => void) {
    
    req.auth = await this.auth.create(req);
    next();
  }
}
