
--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `description`, `creation_date`, `is_active`) VALUES
(1, 'menu_users', 'Menú Usuarios', '2020-06-05 21:09:32', 1),
(2, 'menu_enterprises', 'Menú Mis empresas', '2020-06-20 10:24:33', 1),
(3, 'enterprise_create', 'Crear una empresa', '2020-06-24 01:17:23', 1);

--
-- Dumping data for table `auth_role`
--

INSERT INTO `auth_role` (`id`, `name`, `description`, `creation_date`, `is_active`) VALUES
(1, 'default', 'Default', '2020-06-04 00:29:44', 0),
(2, 'superadmin', 'Superadmin', '2020-06-04 00:29:44', 0);

INSERT INTO `auth_menu` (`id`, `name`, `description`, `creation_date`, `is_active`, `permissionId`) VALUES
(1, 'Usuarios', '', '2020-06-06 01:54:28', 1, 1),
(2, 'Empresas', '/enterprises', '2020-06-20 10:25:01', 1, 2);

--
-- Dumping data for table `auth_role_permissions_auth_permission`
--

INSERT INTO `auth_role_permissions_auth_permission` (`authRoleId`, `authPermissionId`) VALUES
(1, 1),
(2, 1),
(2, 2),
(2, 3);

--
-- Dumping data for table `bus_enterprise_role`
--

INSERT INTO `bus_enterprise_role` (`id`, `name`, `description`, `creation_date`, `is_active`) VALUES
(1, 'superadmin', 'Do whatever he wants', '2020-06-24 01:04:39', 1);

--
-- Dumping data for table `mail`
--

INSERT INTO `user` (`id`, `roleId`, `firstname`, `lastname`, `account`, `email`, `is_email_valid`, `phone`, `is_phone_valid`, `password`, `creation_date`, `is_active`) VALUES
(1, 2, 'Test', 'Test', '__test__', 'test@test.test', 1, NULL, 0, '$2b$10$5ZDt85s6moyvlB5tPvD8zeSCtXMM1BPumlvAvwpR35jkcj8jH7ruW', '2020-06-24 02:34:26', 1);
