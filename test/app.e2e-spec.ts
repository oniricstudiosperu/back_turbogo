import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { async } from 'rxjs/internal/scheduler/async';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let httpServer;
  let remote:boolean = false;
  let APIURL = 'http://localhost:3200';
  let API:any = remote ? () => APIURL : () => app.getHttpServer();
  

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
    .overrideProvider(AppModule)
    .useValue(AppModule)
    .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    httpServer = app.getHttpServer();
  });

  it('/ (GET)', async () => {
    await request(API())
      .get('/')
      .expect(200)
      .expect('Hello World!');
    
  });

  

  afterAll (async () => {
    await app.close();
  });
});
