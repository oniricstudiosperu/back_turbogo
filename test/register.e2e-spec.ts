import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { async } from 'rxjs/internal/scheduler/async';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let httpServer;
  let remote:boolean = false;
  let APIURL = 'http://localhost:3000';
  let API:any = remote ? () => APIURL : () => app.getHttpServer();
  

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
    .overrideProvider(AppModule)
    .useValue(AppModule)
    .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    httpServer = app.getHttpServer();
  });

  it ('/ (POST)', async () => {
    await request(API())
      .post('/register')
      .expect(400)
      .expect('{"statusCode":400,"error":{"firstname":"Debe estar definido"}}');
  })

  it ('/ (POST) { type, password }', async () => {
    await request(API())
      .post('/register')
      .send({
        firstname: 'asdasdl',
        password: 'asdasdsad'
      })
      .expect(400)
      .expect ('{"statusCode":400,"error":{"lastname":"Debe estar definido"}}');
  })

  it ('/ (POST) { type, email, password }', async () => {
    await request(API())
      .post('/register')
      .send({
        account: 'asjdlsad',
        password: 'asdasdsad'
      })
      .expect(400)
      .expect ('{"statusCode":400,"error":{"firstname":"Debe estar definido"}}');
  })

  afterAll (async () => {
    await app.close();
  });
});
